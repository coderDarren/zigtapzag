﻿using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;

namespace RC_Projects {
	namespace ZigZag {

		/// <summary>
		/// The Session Manager is the entry point to the game
		/// </summary>
		public class SessionManager : MonoBehaviour {

			public static SessionManager Instance;

			public string userId { get; private set; }

			GameManager game;

			/// <summary>
			/// Singleton pattern. Only one Session Manager allowed
			/// </summary>
			void Awake() {
				if (Instance != null) {
					Destroy(gameObject);
				}
				else {
					Instance = this;
					DontDestroyOnLoad(gameObject);
				}
			}

			/// <summary>
			/// Begin loading a user
			/// </summary>
			void Start() {
				game = GameManager.Instance;
				DataStorage.LoadUser(userId, false);
				StartCoroutine("WaitToStart");
			}


			/// <summary>
			/// Wait until all player data has been loaded before starting the game
			/// </summary>
			IEnumerator WaitToStart() {
				while (DataStorage.LOADING_USER) {
					yield return null;
				}
				game.StartApp();
			}


		}
	}
}
