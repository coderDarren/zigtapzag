﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RC_Projects {
	namespace ZigZag {

		/// <summary>
		/// The progress manager is the class the game may access to modify User Data..
		/// </summary>
		public class ProgressManager : MonoBehaviour {

			public static ProgressManager Instance;

			public int Attempts { get { return DataStorage.GetLocalData(GPGSIds.event_attempts); } }
			public int HighScore { get { return DataStorage.GetLocalData(GPGSIds.leaderboard_high_score); } }
			public int Score { get { return score; } }
			public bool AudioOn { 
				get { 
				#if UNITY_WEBGL
					return true;
				#endif
					return DataStorage.GetLocalData("Audio") == 1 ? true : false; 
				} 
			}

			int score = 0;

			/// <summary>
			/// Singleton pattern. Only one Progress Manager allowed
			/// </summary>
			void Awake() {
				if (Instance != null) {
					Destroy(gameObject);
				}
				else {
					Instance = this;
					DontDestroyOnLoad(gameObject);
				}
			}

			/// <summary>
			/// Saves audio state locally
			/// </summary>
			public void ToggleAudio() {
				if (AudioOn) {
					DataStorage.SaveLocalData("Audio", 0);
				}
				else {
					DataStorage.SaveLocalData("Audio", 1);
				}
			}
			
			/// <summary>
			/// Reset score
			/// </summary>
			public void ResetScore() {
				score = 0;
			}

			/// <summary>
			/// Add to current session's score, and report to the leaderboard if a new high score is reached
			/// Determine if an achievement should be unlocked
			/// </summary>
			public void AddScore(int value) {
				score += value;
				if (score > HighScore) {
					DataStorage.SaveLocalData(GPGSIds.leaderboard_high_score, score);
				}
			}

			/// <summary>
			/// Add to number of session attempts and save the value
			/// </summary>
			public void IncrementAttempts() {
				DataStorage.SaveLocalData(GPGSIds.event_attempts, Attempts + 1);
			}

			
		}
	}
}