﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

#if UNITY_ANDROID
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Events;
#endif

namespace RC_Projects {
	namespace ZigZag {

		/// <summary>
		/// Data Storage is your access point to data management
		/// The functions in this class should only be called by ProgressManager.cs
		/// </summary>
		public class DataStorage {

			public static bool LOADING_USER 
			{
				get { 
					if (!initialLoadBegan) return true;
					return loadJobs > 0 ? true : false; 
				}
			}

			static string INITIALIZED = "INITIALIZED";
			static string USER_ID;

			static int loadJobs = 0;
			static bool initialLoadBegan = false;
			
			/// <summary>
			/// Only called once per user.
			/// When a new username is passed to this function, a check is done to see if her data has been initialized
			/// If not, then a few fetch operations will pull the data and save it locally with player prefs
			/// </summary>
			public static void LoadUser(string user, bool reset) {

				initialLoadBegan = true;
				USER_ID = user;		

				//determine if this user has data stored on this device
				int localStorageStatus = PlayerPrefs.GetInt(USER_ID+INITIALIZED);

				if (localStorageStatus != 1 || reset) {			

					SaveLocalData("Audio", 1);
					SaveLocalData(GPGSIds.event_attempts, 0);
					SaveLocalData(GPGSIds.leaderboard_high_score, 0);
					if (USER_ID != string.Empty) loadJobs += 3;

					PlayerPrefs.SetInt(USER_ID+INITIALIZED, 1);			
				}
			}

			/// <summary>
			/// Retrieve a value from local data
			/// </summary>
			public static int GetLocalData(string dataId) {
				int ret = PlayerPrefs.GetInt(USER_ID+dataId);
				return ret;
			}

			/// <summary>
			/// Save a value to local
			/// </summary>
			public static void SaveLocalData(string dataId, int amount) {
				loadJobs--;
				PlayerPrefs.SetInt(USER_ID+dataId, amount);
			}

		}
	}
}
